import circle.circle;

public class App {
    public static void main(String[] args) throws Exception {

        circle  circle1 = new circle();
        circle circle2 = new circle(3.0);

        System.out.println(circle1.toString());
        System.out.println(circle2.toString());

        System.out.println(circle1.getArea());
        System.out.println(circle2.getArea());

        System.out.println(circle1.getCircumference());
        System.out.println(circle2.getCircumference());



    }
}
